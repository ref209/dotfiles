#!/bin/bash

BASEDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# neovim
ln -s -f ${BASEDIR}/nvim/init.lua ~/.config/nvim/init.lua

# tmux
ln -s -f ${BASEDIR}/tmux/.tmux.conf ~/.tmux.conf

# alacritty
ln -s -f ${BASEDIR}/alacritty/alacritty.yml ~/.config/alacritty/alacritty.yml
